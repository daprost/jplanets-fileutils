package de.jplanets.helper.filefilterlist;

import java.io.File;

import org.apache.log4j.BasicConfigurator;

import de.jplanets.helper.filefilter.NameEqualsFileFilter;
import de.jplanets.helper.filefilterlist.AbstractListFileFilter;

import junit.framework.TestCase;

public class ByNameFileFilterTest extends TestCase {
  static {
  BasicConfigurator.configure();
  }
  public void testMatch() {
    // 'Sag mir, ob die Datei in der liste ist'
    StringFileFilterList f = new StringFileFilterList(new NameEqualsFileFilter());
    f.setBreakMatch(true);
    f.setBreakResult(true);
    f.add("uli.abc");
    f.add("uli.def");
    assertTrue(f.accept(new File("uli.def")));
    assertFalse(f.accept(new File("uli.xyz")));
  }
  public void testNoMatch() {
    // 'Sag mir, ob die Datei nicht in der Liste ist'
    StringFileFilterList f = new StringFileFilterList(new NameEqualsFileFilter());
    f.setBreakMatch(true);
    f.setBreakResult(false);
    f.add("uli.ghi");
    f.add("uli.jkl");
    assertTrue(f.accept(new File("uli.uvw")));
    assertFalse(f.accept(new File("uli.jkl")));
  }
}

package de.jplanets.helper.filefilterlist;

import java.io.File;

import junit.framework.TestCase;

import org.apache.log4j.BasicConfigurator;

import de.jplanets.helper.filefilter.NameEndsWithFileFilter;
import de.jplanets.helper.filefilter.NameEqualsFileFilter;

public class FileFilterChainTest extends TestCase {
  static {
  BasicConfigurator.configure();
  }
  public void testTestAccepting() {
    NameEqualsFileFilter f = new NameEqualsFileFilter();
    f.setString("uli.abc");
    //f.add("uli.def");
    NameEndsWithFileFilter f1 = new NameEndsWithFileFilter();
    f1.setString("abc");
    //f1.add("def");
    FileFilterChain c = new FileFilterChain();
    c.add(f);
    c.add(f1);
    assertTrue(c.accept(new File("uli.abc")));
    System.out.println("false=" + (false == true));
    System.out.println("true=" + (false == false));
    System.out.println("false=" + (true == false));
    System.out.println("true=" + (true == true));
    //assertFalse(c.accept(new File("uli.xyz")));
  }
  public void tesTestDeclining() {
    StringFileFilterList f = new StringFileFilterList(new NameEqualsFileFilter());
//    f.setMatchMode(AbstractListFileFilter.MODE_DECLINIG);
    f.add("uli.ghi");
    f.add("uli.jkl");
    StringFileFilterList f1 = new StringFileFilterList(new NameEndsWithFileFilter());
//    f1.setMatchMode(AbstractListFileFilter.MODE_DECLINIG);
    f1.add("ghi");
    f1.add("jkl");
    FileFilterChain c = new FileFilterChain();
//    c.setMatchMode(AbstractListFileFilter.MODE_ACCEPTING);
    c.add(f);
    c.add(f1);
    assertTrue(c.accept(new File("uli.uvw")));
    assertFalse(c.accept(new File("uli.jkl")));
  }
}

package de.jplanets.helper.filefilterlist;

import java.io.File;

import org.apache.log4j.BasicConfigurator;

import de.jplanets.helper.filefilter.NameEndsWithFileFilter;
import de.jplanets.helper.filefilter.NameEqualsFileFilter;

import junit.framework.TestCase;

public class ByEndingsFileFilterListTest extends TestCase {
  static {
  BasicConfigurator.configure();
  }
  public void testTestAccepting() {
    StringFileFilterList f = new StringFileFilterList(new NameEndsWithFileFilter());
    // 'Sag mir, ob die Datei in der liste ist'
    f.setBreakMatch(true);
    f.setBreakResult(true);
    f.add("abc");
    f.add("def");
    assertTrue(f.accept(new File("uli.abc")));
    assertFalse(f.accept(new File("uli.xyz")));
  }
  public void testTestDeclining() {
    StringFileFilterList f = new StringFileFilterList(new NameEndsWithFileFilter());
    // 'Sag mir, ob die Datei nicht in der Liste ist'
    f.setBreakMatch(true);
    f.setBreakResult(false);
    f.add("ghi");
    f.add("jkl");
    assertTrue(f.accept(new File("uli.uvw")));
    assertFalse(f.accept(new File("uli.jkl")));
  }
}

/**
 * 
 */
package de.jplanets.helper.filefilter;

import java.io.File;

import de.jplanets.logging.Logger;

/**
 * A {@link StringFileFilter} accepting Files with the given ending.
 * 
 * @author uli
 */
public class NameEndsWithFileFilter extends AbstractStringFileFilter {
  /**
   * The logger.
   */
  private static final Logger _log = Logger
      .getLogger(NameEndsWithFileFilter.class.getName());

  public boolean accept(File pathname) {
    _log.debug("test ending $$ on $$", _s, pathname); //$NON-NLS-1$
    if (_caseSensitive) {
      return pathname.getName().endsWith(_s);
    }
    return pathname.getName().toUpperCase().endsWith(_s.toUpperCase());
  }

}

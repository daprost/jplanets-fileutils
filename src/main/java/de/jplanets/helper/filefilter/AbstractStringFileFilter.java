package de.jplanets.helper.filefilter;

/**
 * This is a abstract base class implementing {@link StringFileFilter}.
 * 
 * @author uli
 */
public abstract class AbstractStringFileFilter implements StringFileFilter {
  /**
   * The String used to filter.
   */
  protected String _s;

  /**
   * The flag for case sensitivity.
   */
  protected boolean _caseSensitive = true;

  /**
   * The flag to tell if case sensitivity is supported.
   */
  protected boolean _supCase = true;

  public void setString(final String s) {
    _s = s;
  }

  public String getString() {
    return _s;
  }

  public void setCaseSensitive(final boolean b) {
    _caseSensitive = b;
  }

  public boolean isCaseSensitive() {
    return _caseSensitive;
  }

  public boolean supportsCaseSensitivity() {
    return _supCase;
  }

}

package de.jplanets.helper.filefilter;

import java.io.File;
import java.io.FileFilter;

/**
 * FileFilter testing on the File.length(). The bigger flag indicates the test
 * result. If true the filter accepts Files with the length greater than the
 * given size.
 * 
 * @author uli
 */
public class SizeFileFilter implements FileFilter {

  /**
   * Create a new SizeFileFilter.
   */
  public SizeFileFilter() {
    // default
  }

  /**
   * Create a new SizeFileFilter configuring it.
   * 
   * @param size the size to compare with.
   * @param bigger the compare mode.
   */
  public SizeFileFilter(final long size, final boolean bigger) {
    _size = size;
    _bigger = bigger;
  }

  /**
   * The size to compare with.
   */
  private long _size = 0L;

  /**
   * The testing mode.
   */
  private boolean _bigger = true;

  public boolean accept(File pathname) {
    if (pathname.length() > _size) {
      return _bigger;
    }
    return !_bigger;
  }

  /**
   * Get the testing mode.
   * 
   * @return the testing mode.
   */
  public boolean isBigger() {
    return _bigger;
  }

  /**
   * Set the testing mode.
   * 
   * @param bigger the new testing mode.
   */
  public void setBigger(boolean bigger) {
    this._bigger = bigger;
  }

  /**
   * Get the size to compare.
   * 
   * @return the size.
   */
  public long getSize() {
    return _size;
  }

  /**
   * Set the size to compare.
   * 
   * @param size the new size.
   */
  public void setSize(long size) {
    this._size = size;
  }

}

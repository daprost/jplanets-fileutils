package de.jplanets.helper.filefilter;

import java.io.File;
import java.io.FileFilter;

/**
 * Implementation of a FileFilter accepting only Directories or only Files.
 * 
 * @author uli
 */
public class FileOrDirFileFilter implements FileFilter {

  /**
   * Flag if this is for files or dirs.
   */
  private boolean _acceptDirs = true;

  /**
   * Create a new instance. Default accepts dirs.
   */
  public FileOrDirFileFilter() {
    // nothing to do
  }

  /**
   * Create a new instance setting acceptance mode.
   * 
   * @param acceptDirs the acceptance mode.
   */
  public FileOrDirFileFilter(final boolean acceptDirs) {
    setDirFilter(acceptDirs);
  }

  public boolean accept(File pathname) {
    if (pathname.isDirectory()) {
      return _acceptDirs;
    }
    return !_acceptDirs;
  }

  /**
   * Set the acceptance mode.
   * 
   * @param acceptDirs the acceptance mode. if true than this accepts only dirs.
   */
  public void setDirFilter(final boolean acceptDirs) {
    _acceptDirs = acceptDirs;
  }

  /**
   * Get the acceptance mode.
   * 
   * @return true if this accepts dirs.
   */
  public boolean isDirFilter() {
    return _acceptDirs;
  }
}

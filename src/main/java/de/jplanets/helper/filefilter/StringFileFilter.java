/**
 * 
 */
package de.jplanets.helper.filefilter;

import java.io.FileFilter;

/**
 * Extension to FileFilter to do tests against Strings. Implementations which
 * allow to switch between case sensitive or not must return true on
 * supportsCaseSensitivity(). Implementations which do not allow this must
 * return false, ignore setCaseSensitivity and return the correct value on
 * isCaseSensitive();
 * 
 * @author uli
 */
public interface StringFileFilter extends FileFilter {
  /**
   * Set the String to do the tests with.
   * 
   * @param s the test String.
   */
  public void setString(final String s);

  /**
   * Get the current test String.
   * 
   * @return the test String
   */
  public String getString();

  /**
   * Set if the filter should be case sensitve. Can be ignored on
   * implementations which do not support case sensitivity.
   * 
   * @param b the new behaviour.
   */
  public void setCaseSensitive(final boolean b);

  /**
   * Tell if the filter is case sensitve or not.
   * 
   * @return true if the filter is case sensitive.
   */
  public boolean isCaseSensitive();

  /**
   * Tell if this filter allows to switch between case sensitivity or not.
   * 
   * @return true if the filter allows to switch.
   */
  public boolean supportsCaseSensitivity();

}

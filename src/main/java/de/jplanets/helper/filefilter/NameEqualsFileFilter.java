/**
 * 
 */
package de.jplanets.helper.filefilter;

import java.io.File;

import de.jplanets.logging.Logger;

/**
 * Implementaition of {@link StringFileFilter} accepting Files with the given
 * name.
 * 
 * @author uli
 */
public class NameEqualsFileFilter extends AbstractStringFileFilter {
  /**
   * The logger.
   */
  private static final Logger _log = Logger
      .getLogger(NameEqualsFileFilter.class.getName());

  /**
   * @see java.io.FileFilter#accept(java.io.File)
   */
  public boolean accept(File pathname) {
    _log.debug("test name $$ on $$", _s, pathname); //$NON-NLS-1$
    if (_caseSensitive) {
      return pathname.getName().equals(_s);
    }
    return pathname.getName().toUpperCase().equals(_s.toUpperCase());
  }

}

package de.jplanets.helper.filefilterlist;

import java.io.File;

import de.jplanets.helper.filefilter.StringFileFilter;
import de.jplanets.logging.Logger;

/**
 * Extension of AbstractListFileFilter using a StringFileFilter to test the elements.
 * 
 * @author uli
 */
public class StringFileFilterList extends AbstractListFileFilter<String> {
  /**
   * The logger.
   */
  private static final Logger _log = Logger
      .getLogger(StringFileFilterList.class.getName());

  /**
   * Create a new instance without a filter.
   */
  public StringFileFilterList() {
    // nothing to do
  }

  /**
   * Create a new instance using the given filter.
   * @param filter the filter to use.
   */
  public StringFileFilterList(StringFileFilter filter) {
    _filter = filter;
  }

  /**
   * The filter to use.
   */
  private StringFileFilter _filter = null;

  /**
   * Initializes the filter with the given String and calls accept() afterwards.
   * {@inheritDoc}
   */
  @Override
  public boolean accept(final String s, final File pathname) {
    _log.debug("Got $$ on $$", s, pathname); //$NON-NLS-1$
    _filter.setString(s);
    return _filter.accept(pathname);
  }

  /**
   * Get the internal filter.
   * @return the filter
   */
  public StringFileFilter getFilter() {
    return _filter;
  }

  /**
   * Set the internal filter.
   * @param filter the filter to set
   */
  public void setFilter(StringFileFilter filter) {
    _filter = filter;
  }

}

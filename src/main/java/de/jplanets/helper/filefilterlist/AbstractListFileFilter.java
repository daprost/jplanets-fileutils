package de.jplanets.helper.filefilterlist;

import java.io.File;
import java.io.FileFilter;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import de.jplanets.logging.Logger;

/**
 * This is the abstract base of a List implementing a FileFilter. With each element of the list the accept(element,
 * pathname) is called. Depending on the matching the next filter is called or the loop is left. Depending on the wanted
 * result it is returned (or the inverse if no match happend). <br>
 * Get true if the file passes a test ("is in the list"): both true. <br>
 * Get true if the file passes no test ("is not the list"): match=true; result=false. <br>
 * Get true if the file passes all tests: match=false; result=false.
 * <br>
 * If the list is empty, it is handled as no match.
 * 
 * @author uli
 * @param <T> The types to be stored in the List.
 */
public abstract class AbstractListFileFilter<T> implements FileFilter, List<T> {
  /**
   * The logger.
   */
  private static final Logger _log = Logger
      .getLogger(AbstractListFileFilter.class.getName());

  /**
   * A List for internal use.
   */
  protected List<T> _list = new Vector<T>();

  /**
   * Flag for the break mode. If true, the filter must return true to stop the
   * checks and return the !passResult.
   */
  protected boolean _breakMatch = true;

  /**
   * Value that is returned on a match.
   */
  protected boolean _breakResult = true;

  /**
   * Set if the loop should break on an accepted filter.
   * 
   * @param b true if the break should occur on an accepted filter.
   */
  public void setBreakMatch(boolean b) {
    _breakMatch = b;
  }

  /**
   * Get the break flag.
   * @return the break flag.
   */
  public boolean getBreakMatch() {
    return _breakMatch;
  }

  /**
   * Set what the accept method should retrun on a match.
   * @param b The return value.
   */
  public void setBreakResult(boolean b) {
    _breakResult = b;
  }

  /**
   * Get the return value on a match.
   * @return the return value on a match.
   */
  public boolean getBreakResult() {
    return _breakResult;
  }

  /**
   * Called for each loop over the list of elements.
   * 
   * @param element The current element.
   * @param pathname The current pathnamen.
   * @return Depending on the matching mode and filter implementation.
   */
  protected abstract boolean accept(T element, File pathname);

  /**
   * Loops over the internal list calling the accept(T element, File pahtname)
   * for each element in the list. If one filter accepts, the match mode is
   * returned. {@inheritDoc}
   */
  public final boolean accept(File pathname) {
    for (T element : _list) {
      if (_breakMatch == accept(element, pathname)) {
        _log.debug("match in", this.getClass().getName()); //$NON-NLS-1$
        return _breakResult;
      }
    }
    _log.debug("no match in", this.getClass().getName()); //$NON-NLS-1$
    return !_breakResult;
  }

  // overwrite all the List methods
  public boolean add(T s) {
    return _list.add(s);
  }

  public void add(int index, T element) {
    _list.add(index, element);
  }

  public boolean addAll(Collection<? extends T> c) {
    return _list.addAll(c);
  }

  public boolean addAll(int index, Collection<? extends T> c) {
    return _list.addAll(index, c);
  }

  public void clear() {
    _list.clear();
  }

  public boolean contains(Object o) {
    return _list.contains(o);
  }

  public boolean containsAll(Collection<?> c) {
    return _list.containsAll(c);
  }

  public T get(int index) {
    return _list.get(index);
  }

  public int indexOf(Object o) {
    return _list.indexOf(o);
  }

  public boolean isEmpty() {
    return _list.isEmpty();
  }

  public Iterator<T> iterator() {
    return _list.iterator();
  }

  public int lastIndexOf(Object o) {
    return _list.lastIndexOf(o);
  }

  public ListIterator<T> listIterator() {
    return _list.listIterator();
  }

  public ListIterator<T> listIterator(int index) {
    return _list.listIterator(index);
  }

  public boolean remove(Object o) {
    return _list.remove(o);
  }

  public T remove(int index) {
    return _list.remove(index);
  }

  public boolean removeAll(Collection<?> c) {
    return _list.removeAll(c);
  }

  public boolean retainAll(Collection<?> c) {
    return _list.retainAll(c);
  }

  public T set(int index, T element) {
    return _list.set(index, element);
  }

  public int size() {
    return _list.size();
  }

  public List<T> subList(int fromIndex, int toIndex) {
    return _list.subList(fromIndex, toIndex);
  }

  public Object[] toArray() {
    return _list.toArray();
  }

  @SuppressWarnings("hiding")
  public <T> T[] toArray(T[] a) {
    return _list.toArray(a);
  }

}

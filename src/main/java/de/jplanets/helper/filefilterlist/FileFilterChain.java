package de.jplanets.helper.filefilterlist;

import java.io.File;
import java.io.FileFilter;

import de.jplanets.logging.Logger;

/**
 * A Chain for FileFilters. Default tests all filters if they result in true and returns true then.
 * 
 * @author uli
 */
public class FileFilterChain extends AbstractListFileFilter<FileFilter> {
  /**
   * The logger.
   */
  private static final Logger _log = Logger.getLogger(FileFilterChain.class
      .getName());

  /**
   * Create a default file filter chain.
   */
  public FileFilterChain() {
    _breakMatch = false;
    _breakResult = false;
  }

  /**
   * Use the filter.
   * 
   * @param filter The filter to use.
   * @param pathname The file to test against.
   * @return true if the filter succeeds.
   */
  @Override
  public boolean accept(FileFilter filter, final File pathname) {
    _log.debug("Using filter $$ on $$", filter, pathname); //$NON-NLS-1$
    return filter.accept(pathname);
  }
}

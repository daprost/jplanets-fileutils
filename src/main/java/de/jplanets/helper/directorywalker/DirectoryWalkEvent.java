package de.jplanets.helper.directorywalker;

import java.io.File;
import java.util.EventObject;

public class DirectoryWalkEvent extends EventObject {
  public static int WALK_START = 0;
  public static int ENTER_DIRECTORY = 1;
  public static int START_DIRECTORY = 2;
  public static int GOT_FILE = 3;
  public static int END_DIRECTORY = 4;
  public static int WALK_END = 5;
  
  private int _eventid = -1;
  private boolean _enterDirectory = true;
  private File _file;
  private int _level;
  public DirectoryWalkEvent(Object source) {
    super(source);
  }
  public DirectoryWalkEvent(Object source, int ID, File f, int level) {
    super(source);
    _eventid = ID;
    _file = f;
    _level = level;
  }
  /**
   * @return the _enterDirectory
   */
  public boolean enterDirectory() {
    return _enterDirectory;
  }
  /**
   * @param directory the _enterDirectory to set
   */
  public void setEnterDirectory(boolean directory) {
    _enterDirectory = directory;
  }
  public File getFile() {
    return _file;
  }
  public int getEventID() {
    return _eventid;
  }
}

package de.jplanets.helper.directorywalker;

import java.io.File;
import java.io.IOException;

public interface DirectoryWalker {
 void doWalk(File directory) throws IOException;
  
 void startWalk(File directory);
 // return false if dir (and subdirs) should be skipped
 boolean enterDirectory(File directory, int depth); 
 void startDirectory(File directory, int depth); 
 void gotFile(File directory, int depth); 
 void endDirectory(File directory, int depth); 
 void endWalk();
 
}

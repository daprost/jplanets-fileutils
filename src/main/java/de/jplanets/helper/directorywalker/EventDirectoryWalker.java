package de.jplanets.helper.directorywalker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.io.filefilter.IOFileFilter;

public class EventDirectoryWalker extends org.apache.commons.io.DirectoryWalkerII {
  public EventDirectoryWalker() {
    super();
  }
  public EventDirectoryWalker(Comparator c) {
    super(null, -1, c);
  }
  public EventDirectoryWalker(IOFileFilter directoryFilter, IOFileFilter fileFilter, int depthLimit, Comparator comparator) {
    super(directoryFilter, fileFilter, depthLimit, comparator);
  }

  private List<DirectoryWalkListener> _listener = new ArrayList<DirectoryWalkListener>();
  private File _baseDir = null;
  /* (non-Javadoc)
   * @see org.apache.commons.io.DirectoryWalker#handleDirectory(java.io.File, int, java.util.Collection)
   */
  @Override
  protected boolean handleDirectory(File arg0, int arg1, Collection arg2) throws IOException {
    DirectoryWalkEvent e = new DirectoryWalkEvent(this, DirectoryWalkEvent.ENTER_DIRECTORY, arg0, arg1);
    informListeners(e);
    return e.enterDirectory();
  }

  /* (non-Javadoc)
   * @see org.apache.commons.io.DirectoryWalker#handleDirectoryEnd(java.io.File, int, java.util.Collection)
   */
  @Override
  protected void handleDirectoryEnd(File arg0, int arg1, Collection arg2) throws IOException {
    DirectoryWalkEvent e = new DirectoryWalkEvent(this, DirectoryWalkEvent.END_DIRECTORY, arg0, arg1);
    informListeners(e);
  }

  /* (non-Javadoc)
   * @see org.apache.commons.io.DirectoryWalker#handleDirectoryStart(java.io.File, int, java.util.Collection)
   */
  @Override
  protected void handleDirectoryStart(File arg0, int arg1, Collection arg2) throws IOException {
    DirectoryWalkEvent e = new DirectoryWalkEvent(this, DirectoryWalkEvent.START_DIRECTORY, arg0, arg1);
    informListeners(e);
  }

  /* (non-Javadoc)
   * @see org.apache.commons.io.DirectoryWalker#handleEnd(java.util.Collection)
   */
  @Override
  protected void handleEnd(Collection arg0) throws IOException {
    DirectoryWalkEvent e = new DirectoryWalkEvent(this, DirectoryWalkEvent.WALK_END, _baseDir, 0);
    informListeners(e);
  }

  /* (non-Javadoc)
   * @see org.apache.commons.io.DirectoryWalker#handleFile(java.io.File, int, java.util.Collection)
   */
  @Override
  protected void handleFile(File arg0, int arg1, Collection arg2) throws IOException {
    DirectoryWalkEvent e = new DirectoryWalkEvent(this, DirectoryWalkEvent.GOT_FILE, arg0, arg1);
    informListeners(e);
  }

  
  /* (non-Javadoc)
   * @see org.apache.commons.io.DirectoryWalker#handleStart(java.io.File, java.util.Collection)
   */
  @Override
  protected void handleStart(File arg0, Collection arg1) throws IOException {
    DirectoryWalkEvent e = new DirectoryWalkEvent(this, DirectoryWalkEvent.WALK_START, arg0, 0);
    informListeners(e);
  }
  public void addListener(DirectoryWalkListener l) {
    _listener.add(l);
  }
  private void informListeners(DirectoryWalkEvent e) {
    for (DirectoryWalkListener l: _listener) {
      l.eventOccured(e);
    }
  }
  public void startWalk(File dir) throws IOException {
    _baseDir = new File(dir.getAbsolutePath());
    walk(_baseDir, null);
  }
  
  
}

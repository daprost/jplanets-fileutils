package de.jplanets.helper.directorywalker;

import java.util.EventListener;

public interface DirectoryWalkListener extends EventListener {
  public void eventOccured(DirectoryWalkEvent e);
}

package de.jplanets.helper.directorywalker;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Vector;


public class CommonsDirectoryWalker extends org.apache.commons.io.DirectoryWalker implements DirectoryWalker {
  private Collection result = new Vector();
  public void doWalk(File directory) throws IOException {
    this.walk(directory, result);
  }

  protected void handleStart(File directory, Collection result){
    startWalk(directory);
  }
  public void startWalk(File directory) {
    // Do nothing
  }
  
  protected boolean handleDirectory(File directory, int depth, Collection result) {
    return enterDirectory(directory, depth);
  }
  public boolean enterDirectory(File directory, int depth) {
    return true;
  }
  
  
  protected void handleDirectoryStart(File directory, int depth, Collection result) {
    startDirectory(directory, depth);
  }
  public void startDirectory(File directory, int depth) {
  }


  protected void handleFile(File directory, int depth, Collection result) {
    gotFile(directory, depth);
  }
  public void gotFile(File directory, int depth) {
  }
  
  protected void handleDirectoryEnd(File directory, int depth, Collection result) {
    endDirectory(directory, depth);
  }
  public void endDirectory(File directory, int depth) {
    // TODO Auto-generated method stub
    
  }

  protected void handleEnd(Collection result){
    endWalk();
  }
  public void endWalk() {
    // TODO Auto-generated method stub
    
  }

}

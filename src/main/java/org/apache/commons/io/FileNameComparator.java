package org.apache.commons.io;

import java.io.File;
import java.util.Comparator;

public class FileNameComparator implements Comparator {

  public int compare(Object o1, Object o2) {
    return ((File)o1).getName().compareTo(((File)o2).getName());
  }
}
